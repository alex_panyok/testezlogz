package ezlogz.panok.com.view.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.io.Serializable;
import java.util.List;

import ezlogz.panok.com.R;
import ezlogz.panok.com.model.Mechanizm;
import ezlogz.panok.com.util.Keys;
import ezlogz.panok.com.view.adapter.ListItemAdapter;

final public class ListActivity extends BaseActivity {

    private ListItemAdapter listItemAdapter;



    public static void start(Context context, List<Mechanizm> list){
        Intent intent = new Intent(context, ListActivity.class);
        intent.putExtra(Keys.LIST, (Serializable) list);
        context.startActivity(intent);
    }


    @SuppressWarnings("unchecked")
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(getIntent().getSerializableExtra(Keys.LIST)!=null)
            populateAdapter((List<Mechanizm>) getIntent().getSerializableExtra(Keys.LIST));
    }

    @Override
    void initViews() {
        initRV();
    }


    private void initRV(){
        RecyclerView recyclerView = findViewById(R.id.rvList);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        listItemAdapter= new ListItemAdapter();
        recyclerView.setAdapter(listItemAdapter);
    }

    private void populateAdapter(List<Mechanizm> list){
        listItemAdapter.addAll(list);
    }

    @Override
    int layoutId() {
        return R.layout.activity_list;
    }
}
