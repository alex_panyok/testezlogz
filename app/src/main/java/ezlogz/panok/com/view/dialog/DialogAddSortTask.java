package ezlogz.panok.com.view.dialog;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.AppCompatSpinner;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ArrayAdapter;

import ezlogz.panok.com.R;
import ezlogz.panok.com.model.type.MechanizmType;
import ezlogz.panok.com.sort.type.ArraySizeType;
import ezlogz.panok.com.sort.type.SortType;
import ezlogz.panok.com.view.activity.MainActivity;

final public class DialogAddSortTask extends DialogFragment implements View.OnClickListener {


    private AppCompatSpinner spinnerMechanism;

    private AppCompatSpinner spinnerSort;

    private AppCompatSpinner spinnerSize;


    private ArrayAdapter<MechanizmType> arrayAdapterMechanism;

    private ArrayAdapter<SortType> arrayAdapterSort;

    private ArrayAdapter<ArraySizeType> arrayAdapterSize;


    public static DialogAddSortTask newInstance() {
        return new DialogAddSortTask();
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.dialog_add_sort_task, container, false);
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initViews(view);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        if (dialog.getWindow() != null)
            dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        return dialog;
    }

    private void initSpinners(View view) {
        arrayAdapterMechanism = new ArrayAdapter<>(getActivity(), R.layout.item_spinner, MechanizmType.getAll());
        arrayAdapterMechanism.setDropDownViewResource(R.layout.item_spinner);
        spinnerMechanism = view.findViewById(R.id.spinnerMechanism);
        spinnerMechanism.setAdapter(arrayAdapterMechanism);
        spinnerMechanism.setSelection(0);

        arrayAdapterSort = new ArrayAdapter<>(getActivity(), R.layout.item_spinner, SortType.getAll());
        arrayAdapterSort.setDropDownViewResource(R.layout.item_spinner);
        spinnerSort = view.findViewById(R.id.spinnerSortType);
        spinnerSort.setAdapter(arrayAdapterSort);
        spinnerSort.setSelection(0);


        arrayAdapterSize = new ArrayAdapter<>(getActivity(), R.layout.item_spinner, ArraySizeType.getAll());
        arrayAdapterSize.setDropDownViewResource(R.layout.item_spinner);
        spinnerSize = view.findViewById(R.id.spinnerArraySize);
        spinnerSize.setAdapter(arrayAdapterSize);
        spinnerSize.setSelection(1);

        view.findViewById(R.id.add).setOnClickListener(this);
        view.findViewById(R.id.cancel).setOnClickListener(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (getDialog().getWindow() != null)
            getDialog().getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
    }

    private void initViews(View view) {
        initSpinners(view);
    }

    @Override
    public void onClick(View view) {

        if (getActivity() != null && getActivity() instanceof MainActivity) {
            MainActivity activity = (MainActivity) getActivity();
            switch (view.getId()) {
                case R.id.add:
                    activity.applySort(arrayAdapterMechanism.getItem(spinnerMechanism.getSelectedItemPosition()),
                            arrayAdapterSort.getItem(spinnerSort.getSelectedItemPosition()),
                            arrayAdapterSize.getItem(spinnerSize.getSelectedItemPosition()));
                    dismiss();
                    break;

                case R.id.cancel:
                    dismiss();
                    break;
            }
        }
    }
}
