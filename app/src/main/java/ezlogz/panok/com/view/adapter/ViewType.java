package ezlogz.panok.com.view.adapter;

public interface ViewType {

    int getViewType();
}
