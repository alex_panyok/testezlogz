package ezlogz.panok.com.view.adapter.delegates;


import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import ezlogz.panok.com.R;
import ezlogz.panok.com.model.CompleteSortTask;
import ezlogz.panok.com.model.Mechanizm;
import ezlogz.panok.com.util.TextUtil;

final public class ListItemDelegateAdapter implements ViewTypeDelegateAdapter<Mechanizm,ListItemDelegateAdapter.ListItemHolder> {


    @Override
    public ListItemHolder onCreateViewHolder(ViewGroup parent) {
        return new ListItemHolder(parent);
    }

    @Override
    public void onBindViewHolder(ListItemHolder holder, Mechanizm item) {
        holder.bind(item);
    }




    class ListItemHolder extends RecyclerView.ViewHolder{

        private TextView textViewTitle;


        ListItemHolder(ViewGroup parent) {
            super(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list, parent, false));
            textViewTitle = itemView.findViewById(R.id.title);

        }

        void bind(Mechanizm item){
            textViewTitle.setText(item.getName());
        }






    }

}
