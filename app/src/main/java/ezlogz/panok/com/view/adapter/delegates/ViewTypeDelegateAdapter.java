package ezlogz.panok.com.view.adapter.delegates;


import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

public interface ViewTypeDelegateAdapter<T, VH extends RecyclerView.ViewHolder> {

    VH onCreateViewHolder(ViewGroup parent);

    void onBindViewHolder(VH holder, T item);

}
