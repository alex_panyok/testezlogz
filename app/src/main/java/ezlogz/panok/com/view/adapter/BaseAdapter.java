package ezlogz.panok.com.view.adapter;

import android.support.v4.util.SparseArrayCompat;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import ezlogz.panok.com.view.adapter.delegates.ViewTypeDelegateAdapter;

abstract class BaseAdapter<I extends ViewType> extends RecyclerView.Adapter<RecyclerView.ViewHolder> {



    final ArrayList<I> items = new ArrayList<>();

    final SparseArrayCompat<ViewTypeDelegateAdapter> delegateAdapters = new SparseArrayCompat<>();


    @Override
    public int getItemCount() {
        return items.size();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return delegateAdapters.get(viewType).onCreateViewHolder(parent);
    }

    @SuppressWarnings("unchecked")
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        delegateAdapters.get(getItemViewType(position)).onBindViewHolder(holder, items.get(position));
    }

    @Override
    public long getItemId(int position) {
        return items.get(position).hashCode();
    }

    public void addItem(I item){
        items.add(item);
        notifyDataSetChanged();
    }

    public void addAll(List<I> item){
        items.addAll(item);
        notifyDataSetChanged();
    }

    @Override
    public int getItemViewType(int position) {
        return items.get(position).getViewType();
    }

    abstract void populateDelegateAdapters();
}
