package ezlogz.panok.com.view.adapter;

import ezlogz.panok.com.model.Mechanizm;
import ezlogz.panok.com.util.Keys;
import ezlogz.panok.com.view.adapter.delegates.ListItemDelegateAdapter;

final public class ListItemAdapter extends BaseAdapter<Mechanizm> {


    public ListItemAdapter() {
        populateDelegateAdapters();
    }

    @Override
    void populateDelegateAdapters() {
        delegateAdapters.append(Keys.MECHANIZM_TYPE, new ListItemDelegateAdapter());
    }
}
