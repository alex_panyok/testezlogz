package ezlogz.panok.com.view.adapter;

import ezlogz.panok.com.model.CompleteSortTask;
import ezlogz.panok.com.util.Keys;
import ezlogz.panok.com.view.adapter.delegates.SortResultDelegateAdapter;

final public class SortResultAdapter extends BaseAdapter<CompleteSortTask> {

    private SortResultDelegateAdapter.OnListSelectedListener onListSelectedListener;


    public SortResultAdapter(SortResultDelegateAdapter.OnListSelectedListener onListSelectedListener) {
        this.onListSelectedListener = onListSelectedListener;
        populateDelegateAdapters();

    }

    @Override
    void populateDelegateAdapters() {
        delegateAdapters.append(Keys.COMPLETE_TASK_TYPE, new SortResultDelegateAdapter(onListSelectedListener));
    }






}
