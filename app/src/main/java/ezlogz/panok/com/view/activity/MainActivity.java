package ezlogz.panok.com.view.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import java.util.List;

import ezlogz.panok.com.R;
import ezlogz.panok.com.data.MechanizmDataSource;
import ezlogz.panok.com.model.CompleteSortTask;
import ezlogz.panok.com.model.Mechanizm;
import ezlogz.panok.com.model.SortTask;
import ezlogz.panok.com.model.type.MechanizmType;
import ezlogz.panok.com.service.Service;
import ezlogz.panok.com.sort.type.ArraySizeType;
import ezlogz.panok.com.sort.type.SortType;
import ezlogz.panok.com.util.Keys;
import ezlogz.panok.com.view.adapter.SortResultAdapter;
import ezlogz.panok.com.view.adapter.delegates.SortResultDelegateAdapter;
import ezlogz.panok.com.view.dialog.DialogAddSortTask;


final public class MainActivity extends BaseActivity implements SortResultDelegateAdapter.OnListSelectedListener {


    private RecyclerView recyclerViewSortResult;
    private SortResultAdapter sortResultAdapter;
    private View addNewTaskLayout;
    private BroadcastReceiver sortCompleteReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getSerializableExtra(Keys.SORT_RESULT) instanceof CompleteSortTask) {
                hideAddNewTaskLayout();
                sortResultAdapter.addItem((CompleteSortTask) intent.getSerializableExtra(Keys.SORT_RESULT));
                recyclerViewSortResult.post(new Runnable() {
                    @Override
                    public void run() {
                        recyclerViewSortResult.smoothScrollToPosition(sortResultAdapter.getItemCount() - 1);
                    }
                });

            }
        }
    };


    private MechanizmDataSource mechanizmDataSource;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initDataSource();
        registerReceiver();
    }

    private void initDataSource() {
        mechanizmDataSource = new MechanizmDataSource();
    }


    private void startSortProcess(SortTask sortTask) {
        Intent intent = new Intent(this, Service.class);
        intent.putExtra(Keys.SORT_TASK, sortTask);
        startService(intent);
    }


    public void applySort(MechanizmType mechanizmType, SortType sortType, ArraySizeType type) {
        startSortProcess(new SortTask<>(sortType, mechanizmDataSource.getList(mechanizmType, type), mechanizmType.getTitle()));
    }

    @Override
    void initViews() {
        addNewTaskLayout = findViewById(R.id.addNewTaskLayout);
        initRV();
        initFab();
    }


    private void hideAddNewTaskLayout() {
        if (addNewTaskLayout.getVisibility() == View.VISIBLE) {
            addNewTaskLayout.setVisibility(View.GONE);
        }
    }

    private void initFab() {
        findViewById(R.id.addSortTask).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DialogAddSortTask.newInstance().show(getSupportFragmentManager(), Keys.DIALOG_ADD_TASK);
            }
        });
    }

    private void initRV() {
        recyclerViewSortResult = findViewById(R.id.rvSortResult);
        recyclerViewSortResult.setLayoutManager(new LinearLayoutManager(this));
        sortResultAdapter = new SortResultAdapter(this);
        sortResultAdapter.setHasStableIds(true);
        recyclerViewSortResult.setAdapter(sortResultAdapter);
    }

    @Override
    int layoutId() {
        return R.layout.activity_main;
    }


    @Override
    protected void onDestroy() {
        if (isFinishing()) {
            unregisterReceiverAndStopService();
        }
        super.onDestroy();
    }

    private void unregisterReceiverAndStopService() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(sortCompleteReceiver);
        stopService(new Intent(this, Service.class));
    }


    private void registerReceiver() {
        LocalBroadcastManager.getInstance(this).registerReceiver(
                sortCompleteReceiver, new IntentFilter(Keys.SORT_FILTER));
    }

    @SuppressWarnings("unchecked")
    @Override
    public <E extends Mechanizm> void listSelected(List<E> list) {
        ListActivity.start(this, (List<Mechanizm>) list);
    }
}
