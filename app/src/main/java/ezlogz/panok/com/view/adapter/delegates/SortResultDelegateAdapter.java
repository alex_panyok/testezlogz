package ezlogz.panok.com.view.adapter.delegates;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.util.List;

import ezlogz.panok.com.R;
import ezlogz.panok.com.model.CompleteSortTask;
import ezlogz.panok.com.model.Mechanizm;
import ezlogz.panok.com.util.TextUtil;

final public class SortResultDelegateAdapter implements ViewTypeDelegateAdapter<CompleteSortTask, SortResultDelegateAdapter.SortResultHolder> {

    private OnListSelectedListener onListSelectedListener;

    public SortResultDelegateAdapter(OnListSelectedListener onListSelectedListener) {
        this.onListSelectedListener = onListSelectedListener;
    }

    @Override
    public SortResultHolder onCreateViewHolder(ViewGroup parent) {
        return new SortResultHolder(parent);
    }

    public interface OnListSelectedListener {
        <E extends Mechanizm> void listSelected(List<E> list);
    }


    @Override
    public void onBindViewHolder(SortResultHolder holder, CompleteSortTask item) {
        holder.bind(item);
    }

    class SortResultHolder extends RecyclerView.ViewHolder {

        private TextView textViewListType;
        private TextView textViewSortType;
        private TextView textViewSortTime;

        private Button buttonInitList;
        private Button buttonSortedList;

        SortResultHolder(ViewGroup parent) {
            super(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_sort_result, parent, false));
            textViewListType = itemView.findViewById(R.id.listType);
            textViewSortType = itemView.findViewById(R.id.sortMethod);
            textViewSortTime = itemView.findViewById(R.id.sortTime);

            buttonInitList = itemView.findViewById(R.id.initList);
            buttonSortedList = itemView.findViewById(R.id.sortedList);
        }

        void bind(final CompleteSortTask completeSortTask) {
            Context context = itemView.getContext();
            textViewListType.setText(TextUtil.formatString(context.getString(R.string.list_type_format), completeSortTask.getListType()));
            textViewSortType.setText(TextUtil.formatString(context.getString(R.string.sort_type_format), completeSortTask.getSortType().getTitle()));
            textViewSortTime.setText(TextUtil.formatString(context.getString(R.string.sort_time_format), TextUtil.formatNanoSeconds(context, completeSortTask.getSortTime())));
            View.OnClickListener clickListener = new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (onListSelectedListener != null) {
                        switch (view.getId()) {
                            case R.id.initList: {
                                onListSelectedListener.listSelected(completeSortTask.getInitialList());
                                break;
                            }
                            case R.id.sortedList: {
                                onListSelectedListener.listSelected(completeSortTask.getList());
                                break;
                            }
                        }
                    }
                }
            };
            buttonInitList.setOnClickListener(clickListener);
            buttonSortedList.setOnClickListener(clickListener);
        }


    }

}
