package ezlogz.panok.com.callback;


import ezlogz.panok.com.model.CompleteSortTask;
import ezlogz.panok.com.model.Mechanizm;

public interface Callback {

    <E extends Mechanizm> void sortComplete(CompleteSortTask<E> completeSortTask);
}
