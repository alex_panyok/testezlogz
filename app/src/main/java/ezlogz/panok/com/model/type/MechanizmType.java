package ezlogz.panok.com.model.type;

import java.util.Arrays;
import java.util.List;

public enum MechanizmType {

    CAR("Car"),
    SHIP("Ship"),
    PLANE("Plane");

    private String title;

    MechanizmType(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    @Override
    public String toString() {
        return getTitle();
    }

    public static List<MechanizmType> getAll(){
        return Arrays.asList(CAR, SHIP, PLANE);
    }
}
