package ezlogz.panok.com.model;


import java.io.Serializable;
import java.util.List;

import ezlogz.panok.com.sort.type.SortType;

final public class SortTask<E extends Mechanizm> implements Serializable {

    private SortType sortType;

    private List<E> list;

    private String mechanizmName;

    public SortTask(SortType sortType, List<E> list, String mechanizmName) {
        this.sortType = sortType;
        this.list = list;
        this.mechanizmName = mechanizmName;
    }

    public SortType getSortType() {
        return sortType;
    }


    public List<E> getList() {
        return list;
    }

    public String getMechanizmName() {
        return mechanizmName;
    }
}
