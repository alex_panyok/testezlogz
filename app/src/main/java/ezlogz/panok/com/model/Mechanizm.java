package ezlogz.panok.com.model;


import android.support.annotation.NonNull;

import java.io.Serializable;

import ezlogz.panok.com.util.Keys;
import ezlogz.panok.com.view.adapter.ViewType;

public abstract class Mechanizm implements Comparable<Mechanizm>, Serializable, ViewType {

    protected String name;

    public Mechanizm(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }


    public void setName(String name) {
        this.name = name;
    }

    @Override
    public int compareTo(@NonNull Mechanizm o) {
        return this.getName().compareToIgnoreCase(o.getName());
    }

    @Override
    public String toString() {
        return getClass().getSimpleName()+"{" +
                "name='" + name + '\'' +
                '}';
    }


    @Override
    public int getViewType() {
        return Keys.MECHANIZM_TYPE;
    }
}
