package ezlogz.panok.com.model;


import java.io.Serializable;
import java.util.List;

import ezlogz.panok.com.sort.type.SortType;
import ezlogz.panok.com.util.Keys;
import ezlogz.panok.com.view.adapter.ViewType;

final public class CompleteSortTask<E extends Mechanizm> implements Serializable, ViewType {

    private SortType sortType;

    private List<E> list;

    private List<E> initialList;

    private long sortTime;

    private String listType;

    public CompleteSortTask(SortType sortType, List<E> list, List<E> initialList, long sortTime, String listType) {
        this.sortType = sortType;
        this.list = list;
        this.initialList = initialList;
        this.sortTime = sortTime;
        this.listType = listType;
    }

    public SortType getSortType() {
        return sortType;
    }


    public List<E> getList() {
        return list;
    }


    public long getSortTime() {
        return sortTime;
    }


    public String getListType() {
        return listType;
    }

    public List<E> getInitialList() {
        return initialList;
    }

    @Override
    public String toString() {
        return "CompleteSortTask{" +
                "sortType=" + sortType +
                ", list=" + list +
                ", sortTime=" + sortTime +
                '}';
    }

    @Override
    public int getViewType() {
        return Keys.COMPLETE_TASK_TYPE;
    }
}
