package ezlogz.panok.com.data;

import java.util.ArrayList;
import java.util.List;

import ezlogz.panok.com.model.Car;
import ezlogz.panok.com.model.Mechanizm;
import ezlogz.panok.com.model.Plane;
import ezlogz.panok.com.model.Ship;
import ezlogz.panok.com.model.type.MechanizmType;
import ezlogz.panok.com.sort.type.ArraySizeType;

final public class MechanizmDataSource {


    private static final String[] NAMES = {"Mersedes", "Opel", "Clinton", "Matra", "Bullet", "Ferrari", "Mazda", "BMW", "Buick", "MAN"};

    private final List<Car> fullCarList = new ArrayList<>();

    private final List<Ship> fullShipList = new ArrayList<>();

    private final List<Plane> fullPlaneList = new ArrayList<>();

    public MechanizmDataSource() {
        populateLists();
    }

    public <E extends Mechanizm> List<E> getList(MechanizmType type, ArraySizeType arraySizeType) {
        List<E> list = chooseList(type);
        if (list != null) {
            switch (arraySizeType) {
                case SMALL:
                    return new ArrayList<>(list.subList(0, 2));
                case NORMAL:
                    return new ArrayList<>(list.subList(0, 6));
                case LARGE:
                    return list;
            }
        }
        return null;
    }

    @SuppressWarnings("unchecked")
    private  <E extends Mechanizm> List<E> chooseList(MechanizmType type) {
        switch (type) {
            case CAR:
                return (List<E>) fullCarList;
            case SHIP:
                return (List<E>) fullShipList;
            case PLANE:
                return (List<E>) fullPlaneList;
        }
        return null;
    }


    private void populateLists() {
        for (String name : NAMES) {
            fullCarList.add(new Car(name));
            fullPlaneList.add(new Plane(name));
            fullShipList.add(new Ship(name));
        }
    }
}
