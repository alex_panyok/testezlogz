package ezlogz.panok.com.sort.type;

import java.util.Arrays;
import java.util.List;

public enum ArraySizeType {
    SMALL("Small"),
    NORMAL("Normal"),
    LARGE("Large");


    private String title;

    ArraySizeType(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    @Override
    public String toString() {
        return getTitle();
    }

    public static List<ArraySizeType> getAll(){
        return Arrays.asList(SMALL, NORMAL, LARGE);
    }
}
