package ezlogz.panok.com.sort;


import java.lang.ref.WeakReference;
import java.util.List;

import ezlogz.panok.com.callback.Callback;
import ezlogz.panok.com.model.Mechanizm;
import ezlogz.panok.com.sort.type.SortType;

final public class InsertionSort<E extends Mechanizm>  extends BaseSortedClass<E>  {


    InsertionSort(List<E> list, WeakReference<Callback> callback, String sortClassName) {
        super(list, callback, sortClassName);
    }

    @Override
    SortType getSortType() {
        return SortType.INSERTION;
    }

    @Override
    public  List<E> sort() {
        if(list.isEmpty())
            return list;
        E temp;
        for (int i = 1; i < list.size(); i++) {
            for(int j = i ; j > 0 ; j--){
                if(list.get(j).compareTo(list.get(j-1)) < 0){
                    temp = list.get(j);
                    list.set(j, list.get(j-1));
                    list.set(j-1, temp);
                }
            }
        }
        return list;
    }


}
