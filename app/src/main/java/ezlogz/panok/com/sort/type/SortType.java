package ezlogz.panok.com.sort.type;


import java.util.Arrays;
import java.util.List;

public enum SortType {
    BUBBLE("Bubble"),
    INSERTION("Insertion"),
    NATIVE("Native");

    private String title;

    SortType(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    @Override
    public String toString() {
        return getTitle();
    }

    public static List<SortType> getAll() {
        return Arrays.asList(BUBBLE, INSERTION, NATIVE);
    }
}
