package ezlogz.panok.com.sort;


import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

import ezlogz.panok.com.callback.Callback;
import ezlogz.panok.com.model.CompleteSortTask;
import ezlogz.panok.com.model.Mechanizm;
import ezlogz.panok.com.sort.type.SortType;

public abstract class BaseSortedClass<E extends Mechanizm> implements Callable<List<E>> {

    List<E> list;

    private List<E> initialList;

    private Callback callback;

    private String sortClassName;

    BaseSortedClass(List<E> list, WeakReference<Callback> callback, String sortClassName) {
        this.list = list;
        this.initialList = new ArrayList<>(list);
        this.callback = callback.get();
        this.sortClassName = sortClassName;
    }

    abstract SortType getSortType();


    public abstract List<E> sort();


    @Override
    public List<E> call() throws InterruptedException {
        long startTime = System.nanoTime();
        List<E> sorted = sort();
        long endTime = System.nanoTime();
        if (callback != null)
            callback.sortComplete(new CompleteSortTask<>(getSortType(),
                    sorted,
                    initialList,
                    endTime - startTime,
                    sortClassName
            ));
        return sorted;
    }
}



