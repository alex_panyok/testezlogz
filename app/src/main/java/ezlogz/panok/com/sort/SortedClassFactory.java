package ezlogz.panok.com.sort;


import java.lang.ref.WeakReference;

import ezlogz.panok.com.callback.Callback;
import ezlogz.panok.com.model.Mechanizm;
import ezlogz.panok.com.model.SortTask;

final public class SortedClassFactory {


    public static <E extends Mechanizm> BaseSortedClass<E> create(SortTask<E> sortTask, Callback callback) {
        switch (sortTask.getSortType()) {
            case BUBBLE:
                return new BubbleSort<>(sortTask.getList(), new WeakReference<>(callback), sortTask.getMechanizmName());
            case INSERTION:
                return new InsertionSort<>(sortTask.getList(), new WeakReference<>(callback), sortTask.getMechanizmName());
            case NATIVE:
                return new NativeJavaSort<>(sortTask.getList(), new WeakReference<>(callback), sortTask.getMechanizmName());
        }
        return null;
    }

}



