package ezlogz.panok.com.sort;


import java.lang.ref.WeakReference;
import java.util.Collections;
import java.util.List;

import ezlogz.panok.com.callback.Callback;
import ezlogz.panok.com.model.Mechanizm;
import ezlogz.panok.com.sort.type.SortType;

final public class NativeJavaSort<E extends Mechanizm>  extends BaseSortedClass<E> {


    NativeJavaSort(List<E> list, WeakReference<Callback> callback, String sortClassName) {
        super(list, callback, sortClassName);
    }

    @Override
    SortType getSortType() {
        return SortType.NATIVE;
    }

    @Override
    public List<E> sort() {
        Collections.sort(list);
        return list;
    }


}
