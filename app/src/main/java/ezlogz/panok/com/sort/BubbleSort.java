package ezlogz.panok.com.sort;


import java.lang.ref.WeakReference;
import java.util.List;

import ezlogz.panok.com.callback.Callback;
import ezlogz.panok.com.model.Mechanizm;
import ezlogz.panok.com.sort.type.SortType;

final public class BubbleSort<E extends Mechanizm> extends BaseSortedClass<E> {


    BubbleSort(List<E> list, WeakReference<Callback> callback, String sortClassName) {
        super(list, callback, sortClassName);
    }

    @Override
    SortType getSortType() {
        return SortType.BUBBLE;
    }

    @Override
    public List<E> sort() {
        if(list.isEmpty())
            return list;
        int n = list.size();
        E temp;
        for (int i = 0; i < n; i++) {
            for (int j = 1; j < (n - i); j++) {
                if (list.get(j - 1).compareTo(list.get(j)) > 0) {
                    temp = list.get(j - 1);
                    list.set(j - 1, list.get(j));
                    list.set(j, temp);
                }
            }
        }
        return list;
    }
}




