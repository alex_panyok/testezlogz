package ezlogz.panok.com.service;


import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;

import ezlogz.panok.com.callback.Callback;
import ezlogz.panok.com.model.CompleteSortTask;
import ezlogz.panok.com.model.Mechanizm;
import ezlogz.panok.com.model.SortTask;
import ezlogz.panok.com.util.Keys;


final public class Service extends android.app.Service {

    private static final int NUM_THREADS = 3;



    private ThreadManager threadManager = new ThreadManager(NUM_THREADS, new Callback() {
        @Override
        public <E extends Mechanizm> void sortComplete(CompleteSortTask<E> completeSortTask) {
            sendCompleteSortTask(completeSortTask);
        }
    });

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent != null && intent.getSerializableExtra(Keys.SORT_TASK) instanceof SortTask)
            executeTaskOnThreadManager(((SortTask) intent.getSerializableExtra(Keys.SORT_TASK)));
        return START_NOT_STICKY;
    }


    private void executeTaskOnThreadManager(SortTask sortTask) {
        if (sortTask != null)
            threadManager.executeTask(sortTask);
    }


    private void sendCompleteSortTask(CompleteSortTask completeSortTask) {
        Intent intent = new Intent(Keys.SORT_FILTER);
        intent.putExtra(Keys.SORT_RESULT, completeSortTask);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
