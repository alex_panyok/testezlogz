package ezlogz.panok.com.service;


import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import ezlogz.panok.com.callback.Callback;
import ezlogz.panok.com.model.SortTask;
import ezlogz.panok.com.sort.BaseSortedClass;
import ezlogz.panok.com.sort.SortedClassFactory;

final class ThreadManager {

    private ExecutorService executorService;
    private Callback callback;

    ThreadManager(int numOfThreads, Callback callback) {
        this.executorService = Executors.newFixedThreadPool(numOfThreads);
        this.callback = callback;
    }


    void executeTask(SortTask sortTask) {
        BaseSortedClass sortedClass = SortedClassFactory.create(sortTask, callback);
        if (sortedClass != null) {
            executorService.submit(sortedClass);
        }
    }


}
