package ezlogz.panok.com.util;


import android.content.Context;

import ezlogz.panok.com.R;

public class TextUtil {


    public static String formatNanoSeconds(Context context, long ns) {
        return String.format(context.getString(R.string.time_ns_format), ns);
    }


    public static String formatString(String format, String formatted) {
        return String.format(format, formatted);
    }
}
