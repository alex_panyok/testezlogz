package ezlogz.panok.com.util;


public class Keys {
    public static final String SORT_TASK = "sort_task";

    public static final String SORT_RESULT = "sort_result";

    public static final String SORT_FILTER= "sort_filter";
    public static final String LIST= "list";


    //Adapter keys
    public static final int MECHANIZM_TYPE = 1001;
    public static final int COMPLETE_TASK_TYPE = 1002;


    //Tag keys
    public static final String DIALOG_ADD_TASK = "dialog_add_task";
}
