package ezlogz.panok.com.util;

import java.lang.reflect.ParameterizedType;

public class Utils {

    public static Class getGenericParameterClass(Class actualClass, int parameterIndex) {
        return (Class) ((ParameterizedType) actualClass.getGenericSuperclass()).getActualTypeArguments()[parameterIndex];
    }
}
